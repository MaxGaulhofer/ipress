# == Class itility-wordpress::install
#
# This class is called from itility-wordpress
#
class itility-wordpress::install {

  # Make this a private class
  assert_private("Use of private class ${name} by ${caller_module_name} not allowed")


  package { 'httpd':
    ensure => installed,
  }

  package {'mysql':
    ensure => installed
  }

  package{'php':
    ensure => installed
  }

  package{'itility-wordpress':
    ensure => installed
  }

}
