# == Class itility-wordpress::service
#
# This class is called from itility-wordpress
#
class itility-wordpress::service {

  # Make this a private class
  assert_private()

  assert_private("Use of private class ${name} by ${caller_module_name} not allowed")

  service { 'httpd':
    ensure => running,
    enable => true,
  }

}
