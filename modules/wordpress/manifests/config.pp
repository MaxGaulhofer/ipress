# == Class itility-wordpress::config
#
# This class is called from itility-wordpress
#
class itility-wordpress::config {

  # Make this a private class
  assert_private("Use of private class ${name} by ${caller_module_name} not allowed")

  
}
