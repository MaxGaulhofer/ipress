# == Class ipress::service
#
# This class is called from ipress
#
class ipress::service {

  # Make this a private class
  assert_private()

  assert_private("Use of private class ${name} by ${caller_module_name} not allowed")

}
