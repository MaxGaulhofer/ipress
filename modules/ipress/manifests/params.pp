# == Class ipress::params
#
# This class is called from ipress
# It sets variables according to platform
#
class ipress::params {

  # platform independent variables
  $some_string     = 'started'
  $some_boolean    = true
  $package_version = '1.0'
  $service_name    = 'myservice'
  $some_port       = 8080

  # osfamily dependent variables
  case $::osfamily {
    'RedHat': {
      $some_path    = '/tmp/file.txt'
      $package_name = 'rhel_package'
    }
    'windows': {
      $some_path    = 'C:\Temp\file.txt'
      $package_name = 'win_package'
    }
    default: {
      fail("Module ${module_name} is not supported on ${::osfamily}")
    }
  }
}
