# == Class ipress::install
#
# This class is called from ipress
#
class ipress::install {

  # Make this a private class
  assert_private("Use of private class ${name} by ${caller_module_name} not allowed")
}
