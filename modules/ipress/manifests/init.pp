# == Class: ipress

class ipress (
  String $wp_owner    = 'root',
  String $db_user     = 'root',
  String $db_password = '',
  String $install_dir = '/var/www/html/wordpress/',
  String $db_host     = 'localhost',
){
  include apache
  include apache::mod::php
  include mysql::server
  
  #include wordpress  
  package { 'php-mysql':
  	ensure => installed
  }  

  class { 'wordpress':
    wp_owner    => $wp_owner,
    wp_group    => $wp_owner,
    db_user     => $db_user,
    install_dir => $install_dir,
    db_password => $db_password,
    db_host     => $db_host,
  }

  apache::vhost { 'wordpress.example.com':
    port             => '80',
    docroot          => $install_dir,
  }

  # call the classes that do the real work
  class { 'ipress::install': } ->
  class { 'ipress::config':  } ~>
  class { 'ipress::service': }
}