require 'spec_helper'

describe 'gitclient' do

  default_params = { 'some_required_param' => 'abc', 'some_regex' => '123' }

  describe 'on unsupported operating system' do
    [
      { :osfamily => 'Debian', },
      { :osfamily => 'Solaris', },
    ].each do |f|
      describe "#{f[:osfamily]} #{f[:operatingsystemrelease]}" do
        let(:params) {{ }}
        let(:facts) {{
          :osfamily 		  => f[:osfamily],
        }}
        it { should raise_error(Puppet::Error, /Module gitclient is not supported on #{f[:osfamily]}/) }
      end
    end
  end

  win_array = [
#   There is no real distinction in Windows releases in this module, so testing only 1 is enough
#     { :osfamily => 'windows', :operatingsystemrelease => '2003', :operatingsystem => 'windows', :operatingsystemmajrelease => '2003' },
#     { :osfamily => 'windows', :operatingsystemrelease => '2008', :operatingsystem => 'windows', :operatingsystemmajrelease => '2008' },
#     { :osfamily => 'windows', :operatingsystemrelease => '2008 R2', :operatingsystem => 'windows', :operatingsystemmajrelease => '2008 R2' },
#     { :osfamily => 'windows', :operatingsystemrelease => '2012', :operatingsystem => 'windows', :operatingsystemmajrelease => '2012' },
    { :osfamily => 'windows', :operatingsystemrelease => '2012 R2', :operatingsystem => 'windows', :operatingsystemmajrelease => '2012 R2' },
  ]

  lin_array = [
#   There is no real distinction in Linux releases in this module, so testing only 1 is enough
#     { :osfamily => 'RedHat', :operatingsystemrelease => '6.0', :operatingsystem => 'RedHat', :operatingsystemmajrelease => '6' },
#     { :osfamily => 'RedHat', :operatingsystemrelease => '6.0', :operatingsystem => 'RedHat', :operatingsystemmajrelease => '6' },
    { :osfamily => 'RedHat', :operatingsystemrelease => '7.0', :operatingsystem => 'CentOs', :operatingsystemmajrelease => '7' },
  ]

  #Detect if running rspec on windows or not, unfortunately required to work around path validation bug
  if (/cygwin|mswin|mingw|bccwin|wince|emx/ =~ RUBY_PLATFORM)
    os_array=win_array
  else
    os_array=lin_array
  end

  describe 'on supported operating system' do
    os_array.each do |f|
      describe "#{f[:osfamily]}" do
        let(:facts) {{
          :osfamily  => f[:osfamily],
        }}
        describe "without parameters" do
          let(:params) {{ }}
          #There are manditory paramaters which should be passed, so catalog should not compile
          it { should_not compile.with_all_deps }
        end
        let(:params) {default_params}
        describe 'with only mandatory parameters' do
          describe 'correct some_required_param/some_regex' do
              it { should compile.with_all_deps }
              it { should contain_class('gitclient') }
              it { should contain_class('gitclient::params') }
              it { should contain_class('gitclient::install').that_comes_before('gitclient::config') }
              it { should contain_class('gitclient::config').that_notifies('gitclient::service') }
              it { should contain_class('gitclient::service') }
              if f[:osfamily] == 'windows'
                it { should contain_file('C:\Temp\file.txt').with_ensure('file') }
                it { should contain_acl('C:\Temp\file.txt').with_permissions({'identity' => 'Administrator', 'rights' => ['full']}) }
                it { should_not contain_file('/tmp/file.txt') }
              else
                it { should contain_file('/tmp/file.txt').with({'ensure' => 'file', 'owner' => 'root', 'group' => 'root', 'mode' => '0644',}) }
                it { should_not contain_acl}
              end
              it { should contain_package('gitclient_package').with_ensure('1.0') }
              it { should contain_service('gitclient_service').with_ensure('running').with_enable('auto').with_name('myservice') }
          end
        end
        describe 'empty some_required_param' do
          let(:params) {{ 'some_required_param' => '', 'some_regex' => 'abc123'}}
          it { should raise_error(Puppet::Error, /cannot be empty/) }
        end
        #You should check the right value as well if it's used somewhere, in this case it isn't used, see some_version as an example
        describe 'incorrect some_regex' do
          ['','!#',['abc','def'],{'abc' => 'def'}].each do |value|
            context "value {value}" do
              let(:params) {{ 'some_required_param' => 'abc', 'some_regex' => value}}
              it { should raise_error(Puppet::Error, /may only contain/) }
            end
          end
        end
        describe 'some_string' do
          #You should check the right value as well if it's used somewhere, in this case it isn't used, see some_version as an example
          context 'wrong value' do
            [-1,'','abc',{},['a','b']].each do |str|
              describe "#{str}" do
                let(:params) do default_params.merge(:some_string => str) end
                it { should raise_error(Puppet::Error, /must be either started or stopped/) }
              end
            end
          end
        end
        describe 'some_boolean' do
          #You should check the right value as well if it's used somewhere, in this case it isn't used, see some_version as an example
          context 'wrong value' do
            [-1,'abc',{},['a','b']].each do |bool|
              describe "#{bool}" do
                let(:params) do default_params.merge(:some_boolean => bool) end
                it { should raise_error(Puppet::Error, /is not a boolean./) }
              end
            end
          end
        end
        describe "some_port" do
          #You should check the right value as well if it's used somewhere, in this case it isn't used, see some_version as an example
          context "wrong values" do
            [-1,0,65536,'abc',''].each do |port|
              describe "#{port}" do
                let(:params) do default_params.merge(:some_port => port) end
                it { should raise_error(Puppet::Error, /validate_integer\(\)/) }
              end
            end
          end
        end
        describe "package_version" do
          context 'correct' do
            let(:params) do default_params.merge(:package_version => '2.5') end
            it { should contain_package('gitclient_package').with_ensure('2.5') }
          end
          context "wrong values" do
            [-1,'','a.b'].each do |version|
              describe "#{version}" do
                let(:params) do default_params.merge(:package_version => version) end
                it { should raise_error(Puppet::Error, /must be x\.y/) }
              end
            end
          end
        end
        describe 'some_path' do
          context "correct" do
            if f[:osfamily] == 'windows'
              let(:params) do default_params.merge(:some_path => 'C:\Progs\myfile.txt') end
              it { should contain_file('C:\Progs\myfile.txt') }
              it { should contain_acl('C:\Progs\myfile.txt') }
            else
              let(:params) do default_params.merge(:some_path => '/opt/myfile.txt') end
              it { should contain_file('/opt/myfile.txt') }
            end
          end
          context "not absolute" do
            let(:params) do default_params.merge(:some_path => 'tmp') end
            it { should raise_error(Puppet::Error, /is not an absolute path./) }
          end
        end
      end
    end
  end
end
