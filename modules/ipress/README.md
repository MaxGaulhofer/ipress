#gitclient

####Table of Contents

1. [Overview](#overview)
2. [Module Description](#module-description)
3. [Setup](#setup)
    * [Setup requirements](#setup-requirements)
    * [What gitclient affects](#what-gitclient-affects)
    * [Beginning with gitclient](#beginning-with-gitclient)
4. [Usage](#usage)
    * [Parameters](#parameters)
    * [Examples](#examples)
5. [Reference](#reference)
6. [Limitations](#limitations)
7. [Development](#development)

##Overview
A one-maybe-two sentence summary of what the module does/what problem it solves. This is your 30 second elevator pitch for your module.

##Module Description
If applicable, this section should have a brief description of the technology the module integrates with and what that integration enables. This section should answer the questions: "What does this module do?" and "Why would I use it?".<br />
If your module has a range of functionality (installation, configuration, management, etc.) this is the time to mention it.

##Setup
The basics of getting started with this module.

###Setup Requirements
If your module requires anything extra before setting up (pluginsync enabled, etc.), mention it here. Also mention other module dependencies.

This module requires: 
- [puppetlabs-stdlib](https://github.it.mgt/tooling-automation/puppetlabs-stdlib) (version requirement: >= 4.6.0 <5.0.0)

###What gitclient affects
- A list of files, packages, services, or operations that the module will alter, impact, or execute on the system it is installed on.
- This is a great place to stick any warnings.
- Can be in list or paragraph form.
  
###Beginning with gitclient
The very basic steps needed for a user to get the module up and running. <br />
If your most recent release breaks compatibility or requires particular steps for upgrading, you may wish to include an additional section here: Upgrading (For an example, see http://forge.puppetlabs.com/puppetlabs/firewall).

##Usage
Put the parameters, classes, types, and resources for customizing, configuring, and doing the fancy stuff with your module here. 

###Parameters
This module accepts the following parameters:

####ensure
*Type:* string <br />
*Default:* `present`<br />
*Values:* `present`, `absent` <br/>
*Description:* If the module should make sure everything is present or absent. <br />

####install_dir
*Type:* string <br />
*Default:* `C:\Temp` <br />
*Values:* Any accessible directory. <br />
*Description:* A string containing the installation directory for the package. <br />

####wait
*Type:* integer <br />
*Default:* `undef` <br />
*Values:* `1 - 99` <br />
*Description:* Sets the number of seconds the program should wait. <br />

####manage_service
*Type:* boolean <br />
*Default:* `true` <br />
*Values:* `true`, `false` <br />
*Description:* Decides if the module should manage the service or not. <br />

###Examples

**Example 1:** Minimal default installation
```puppet
  class { 'gitclient': }
```

**Example 2:** Setting the default values for the module
```puppet
  class { 'gitclient':
    ensure         => 'present',
    install_dir    => 'C:\Temp',
    wait           => 60,
    manage_service => true,
  }
```

##Reference
Here, list the classes, types, providers, facts, etc contained in your module. This section should include all of the under-the-hood workings of your module so people know what the module is touching on their system but don't need to mess with things. (We are working on automating this section!)

##Limitations
This is where you list OS compatibility, version compatibility, etc.

##Development
You can contribute by submitting issues, providing feedback and joining the discussions.

Go to: `https://github.it.mgt/tooling-automation/gitclient`

If you want to fix bugs, add new features etc:
- Fork it
- Create a feature branch ( git checkout -b my-new-feature )
- Apply your changes and update rspec tests
- Run rspec tests ( bundle exec rake spec )
- Commit your changes ( git commit -am 'Added some feature' )
- Push to the branch ( git push origin my-new-feature )
- Create new Pull Request
